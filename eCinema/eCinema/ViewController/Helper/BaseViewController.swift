//
//  BaseViewController.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import RxSwift
import UIKit

public typealias completionBlock = (() -> Void)?

@available(iOS 11.0, *)
class BaseViewController: UIViewController, UITextFieldDelegate {
    // MARK: - Properties
    
    var disposeBag = DisposeBag()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    // MARK: - NavBar Properties
    
    enum NavBarButtonType {
        case backButton
    }
    
    enum NavBarColorTheme {
        case white
        case orange
    }
    
    lazy var logoImageView: UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "AppTextName"))
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    lazy var leftNavBarButton: ExtendedTouchAreaButton = {
        let button = ExtendedTouchAreaButton(type: .custom)
        return button
    }()
    
    lazy var navBarWrapperView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    // MARK: - Init
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("don't use this initializer")
    }
    
    deinit {
        print("♻️\(String(describing: Self.self)) was deallocated from memory :: \(Date().description(with: Locale.current)) ::")
    }
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
        setNavBarHidden()
    }
    
    // MARK: - Setup
    
    func setupView() {}
    
    func setupLayout() {}
    
    func setNavBarHidden() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupNavBar(withLeftButtonType leftButtonType: NavBarButtonType?, withRightButtonType rightButtonType: NavBarButtonType?, colorTheme: NavBarColorTheme = .white) {
        view.addSubview(navBarWrapperView)
        navBarWrapperView.addSubview(logoImageView)
        
        if let leftButtonType = leftButtonType {
            var leftButtonImage: UIImage
            switch leftButtonType {
            case .backButton:
                leftButtonImage = #imageLiteral(resourceName: "backBtn.png").withRenderingMode(.alwaysTemplate)
            }
            
            leftNavBarButton.setImage(leftButtonImage, for: .normal)
            leftNavBarButton.imageView?.tintColor = colorTheme == .orange ? UIColor.gold : UIColor.gold
            navBarWrapperView.addSubview(leftNavBarButton)
            
            leftNavBarButton.easy.layout(
                Leading(18).to(view.safeAreaLayoutGuide, .leading),
                CenterY().to(logoImageView)
            )
        }
        
        navBarWrapperView.easy.layout(
            Top().to(view.safeAreaLayoutGuide, .top),
            Leading(),
            Trailing()
        )
        
        logoImageView.easy.layout(
            Top(),
            Width(155),
            Height(45),
            CenterX(),
            Bottom(-20)
        )
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        onReturnKeyPressed(textField)
        return true
    }
    
    public func onReturnKeyPressed(_ textField: UITextField) {}
    
    public func textFieldDidEndEditing(_ textField: UITextField) {}
    
    // MARK: - Touches
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    public func keyboardWillShow(notification: NSNotification) {}
    
    public func keyboardWillHide(notification: NSNotification) {}
    
    // MARK: - Dismiss Handler
    
    public func _dismiss(animated: Bool = true, completion: completionBlock) {
        UIApplication.getTopViewController?.dismiss(animated: animated, completion: completion)
    }
    
    // MARK: - PopVC Handler
    
    func popViewController(animated: Bool, completion: completionBlock) {
        if let topVC = UIApplication.getTopViewController?.navigationController {
            CATransaction.begin()
            CATransaction.setCompletionBlock(completion)
            topVC.popViewController(animated: true)
            CATransaction.commit()
        } else {
            _dismiss(completion: completion)
        }
    }
}
