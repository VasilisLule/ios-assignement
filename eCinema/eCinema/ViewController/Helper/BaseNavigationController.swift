//
//  BaseNavigationController.swift
//  eCinema
//
//  Created by Vasilis Lule on 26/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {
    override func loadView() {
        super.loadView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarHidden(true, animated: false)
    }
}
