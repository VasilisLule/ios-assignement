//
//  RecommendationsScrollView.swift
//  eCinema
//
//  Created by Vasilis Lule on 27/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import UIKit

class RecommendationsScrollView: UIView {
    // MARK: - Properties
    
    private var recomendMovies: [ResultRecomend] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    private var fetchImageSource: [URL] = []
    
    private let containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let spacing: CGFloat = 18
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 35
        layout.minimumInteritemSpacing = 35
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.sectionInset = UIEdgeInsets(top: 3, left: 12, bottom: 3, right: 12)
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.alwaysBounceHorizontal = true
        
        cv.register(RecommendationsOllectionViewCell.self, forCellWithReuseIdentifier: RecommendationsOllectionViewCell.identifier)
        cv.dataSource = self
        cv.delegate = self
        
        return cv
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(containerView)
        containerView.addSubview(collectionView)
    }
    
    private func setupLayout() {
        containerView.easy.layout(
            Edges()
        )
        
        collectionView.easy.layout(
            Top(),
            Height(170),
            Leading(),
            Trailing(),
            Bottom()
        )
    }
    
    func setContent(source: [ResultRecomend]) {
        recomendMovies = source
        recomendMovies.forEach { value in
            let imageURL = ImageBuildPath.getImages(imagePath: value.posterPath)
            self.fetchImageSource.append(imageURL)
        }
    }
}

extension RecommendationsScrollView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recomendMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow: CGFloat = 2
        let spacingBetweenCells: CGFloat = 20
        
        let totalSpacing = (2 * spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        let width = (self.collectionView.bounds.width - totalSpacing) / numberOfItemsPerRow
        
        return CGSize(width: width, height: collectionView.frame.height - 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecommendationsOllectionViewCell.identifier, for: indexPath) as! RecommendationsOllectionViewCell
        
        cell.setContent(imagePath: fetchImageSource[indexPath.row], title: recomendMovies[indexPath.row].title)
        
        return cell
    }
}
