//
//  RecommendationsOllectionViewCell.swift
//  eCinema
//
//  Created by Vasilis Lule on 27/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import UIKit

class RecommendationsOllectionViewCell: UICollectionViewCell {
    public static let identifier = "RecommendationsOllectionViewCell"
    
    // MARK: - Properties
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private let movieImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 8
        return image
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override var bounds: CGRect {
        didSet {
            if bounds != .zero {
                containerView.layer.cornerRadius = 8
                applyShadow(withOpacity: 0.08, shadowSide: .all, shadowRadius: 14, color: .white)
            }
        }
    }
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupView() {
        addSubview(containerView)
        containerView.addSubview(movieImageView)
        containerView.addSubview(titleLabel)
    }
    
    private func setupLayout() {
        containerView.easy.layout(
            Edges(),
            Width(bounds.size.width)
        )
        
        movieImageView.easy.layout(
            Leading(),
            Height(105),
            Trailing(),
            Top()
        )
        
        titleLabel.easy.layout(
            Top().to(movieImageView),
            Leading(5),
            Trailing(5)
        )
    }
    
    func setContent(imagePath: URL, title: String) {
        movieImageView.load(url: imagePath)
        titleLabel.attributedText = NSAttributedString.textBold(title, color: UIColor.white, size: 13)
    }
}
