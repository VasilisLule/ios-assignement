//
//  CastScrollView.swift
//  eCinema
//
//  Created by Vasilis Lule on 27/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import UIKit

class CastScrollView: UIView {
    // MARK: - Properties
    
    private var castMovies: [Cast] = []
    
    private var fetchImageSource: [URL?] = []
    
    private let containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let spacing: CGFloat = 18
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 7)
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear // brown
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.alwaysBounceHorizontal = true
        
        cv.register(CastCollectionViewCell.self, forCellWithReuseIdentifier: CastCollectionViewCell.identifier)
        cv.dataSource = self
        cv.delegate = self
        
        return cv
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(containerView)
        containerView.addSubview(collectionView)
    }
    
    private func setupLayout() {
        containerView.easy.layout(
            Edges()
        )
        
        collectionView.easy.layout(
            Top(),
            Leading(),
            Height(200),
            Trailing(),
            Bottom()
        )
    }
    
    func setContent(source: [Cast]) {
        castMovies.append(contentsOf: source)
        castMovies.forEach { value in
            let imageURL = ImageBuildPath.getImages(imagePath: value.profilePath ?? "")
            if value.profilePath == nil {
                self.fetchImageSource.append(nil)
            } else {
                self.fetchImageSource.append(imageURL)
            }
        }
        collectionView.reloadData()
    }
}

extension CastScrollView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(castMovies.count)
        return castMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow: CGFloat = 3
        let spacingBetweenCells: CGFloat = 20
        
        let totalSpacing = (5 * spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        let width = (self.collectionView.bounds.width - totalSpacing) / numberOfItemsPerRow
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CastCollectionViewCell.identifier, for: indexPath) as! CastCollectionViewCell
        cell.setContent(actor: castMovies[indexPath.row].character, name: castMovies[indexPath.row].name, imagePath: fetchImageSource[indexPath.row])
        
        return cell
    }
}
