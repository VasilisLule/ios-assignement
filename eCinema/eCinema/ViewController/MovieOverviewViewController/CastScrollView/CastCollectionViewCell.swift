//
//  CastCollectionViewCell.swift
//  eCinema
//
//  Created by Vasilis Lule on 26/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import UIKit

class CastCollectionViewCell: UICollectionViewCell {
    public static let identifier = "CastCollectionViewCell"
    
    // MARK: - Properties
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private let actorImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 8
        return image
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private let roleNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override var bounds: CGRect {
        didSet {
            if bounds != .zero {
                containerView.layer.cornerRadius = 8
                applyShadow(withOpacity: 0.08, shadowSide: .all, shadowRadius: 14, color: .white)
            }
        }
    }
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupView() {
        addSubview(containerView)
        containerView.addSubview(actorImageView)
        containerView.addSubview(nameLabel)
        containerView.addSubview(roleNameLabel)
    }
    
    private func setupLayout() {
        containerView.easy.layout(
            Edges(),
            Width(bounds.size.width)
        )
        
        actorImageView.easy.layout(
            Leading(),
            Height(135),
            Trailing(),
            Top()
        )
        
        nameLabel.easy.layout(
            Top().to(actorImageView),
            Leading(5),
            Trailing()
        )
        
        roleNameLabel.easy.layout(
            Top().to(nameLabel),
            Leading(5),
            Trailing()
        )
    }
    
    func setContent(actor: String, name: String, imagePath: URL?) {
        if let url = imagePath {
            actorImageView.load(url: url)
        } else {
            actorImageView.image = #imageLiteral(resourceName: "appicon")
        }
        
        nameLabel.attributedText = NSAttributedString.textBold(name, color: UIColor.darkGreyNew, size: 13, alignment: .left)
        roleNameLabel.attributedText = NSAttributedString.regular(actor, color: UIColor.lightGray, size: 12, alignment: .left)
    }
}
