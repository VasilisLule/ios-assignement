//
//  MovieOverviewViewController.swift
//  eCinema
//
//  Created by Vasilis Lule on 26/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import RxSwift
import UIKit
import youtube_ios_player_helper

@available(iOS 11.0, *)
class MovieOverviewViewController: BaseViewController, YTPlayerViewDelegate {
    // MARK: - UI Properties
    
    var dataResult: Result?
    var videoResult: VideoResult?
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.bounces = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceVertical = true
        return scrollView
    }()
    
    private let containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let backdropImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleToFill
        image.alpha = 0.3
        
        return image
    }()
    
    private let titlelabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private let overvirewPlaceHolderLabel: UILabel = {
        let label = UILabel()
        label.attributedText = NSAttributedString.textBold("Overview", color: .white, size: 18)
        return label
    }()
    
    private let summaryLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private let genreLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private let youtubeView: YTPlayerView = {
        let video = YTPlayerView()
        video.layer.cornerRadius = 10
        video.clipsToBounds = true
        video.contentMode = .scaleAspectFit
        return video
    }()
    
    private let posterImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        image.layer.cornerRadius = 8
        return image
    }()
    
    private let sepView = SeperatorView()
    private let secSepView = SeperatorView()
    
    private let castPlaceHolderLabel: UILabel = {
        let label = UILabel()
        label.attributedText = NSAttributedString.textBold("Movie Cast", color: .white, size: 18)
        return label
    }()
    
    private let castView: CastScrollView = {
        let view = CastScrollView()
        return view
    }()
    
    private let suggestionPlaceHolderLabel: UILabel = {
        let label = UILabel()
        label.attributedText = NSAttributedString.textBold("Recommendations", color: .white, size: 18)
        return label
    }()
    
    private let suggestionView: RecommendationsScrollView = {
        let view = RecommendationsScrollView()
        return view
    }()
    
    var genreName = "" {
        didSet {
            genreLabel.attributedText = NSAttributedString.regular(genreName, color: UIColor.lightGray, size: 17)
        }
    }
    
    // MARK: - Init
    
    init(dataResult: Result) {
        super.init()
        self.dataResult = dataResult
        bindGenresName(dataResult.genreIDS.first!)
        getVideo(movieId: dataResult.id)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentInset = UIEdgeInsets(top: navBarWrapperView.frame.size.height + 10, left: 0, bottom: view.safeAreaInsets.bottom, right: 0)
    }
    
    // MARK: - SetupView
    
    override func setupView() {
        super.setupView()
        view.backgroundColor = .black
        view.addSubview(backdropImageView)
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(posterImageView)
        containerView.addSubview(titlelabel)
        containerView.addSubview(overvirewPlaceHolderLabel)
        containerView.addSubview(summaryLabel)
        containerView.addSubview(genreLabel)
        containerView.addSubview(youtubeView)
        containerView.addSubview(sepView)
        containerView.addSubview(castPlaceHolderLabel)
        containerView.addSubview(castView)
        containerView.addSubview(secSepView)
        containerView.addSubview(suggestionPlaceHolderLabel)
        containerView.addSubview(suggestionView)
        
        setupNavBar(withLeftButtonType: .backButton, withRightButtonType: nil)
        bindUI()
        bindViewModel()
    }
    
    override func setupLayout() {
        super.setupLayout()
        
        scrollView.easy.layout(
            Top(10).to(navBarWrapperView),
            Leading(),
            Trailing(),
            Bottom()
        )
        
        containerView.easy.layout(
            Edges(),
            Width(view.bounds.size.width)
        )
        
        backdropImageView.easy.layout(
            Height(350),
            Leading(),
            Trailing(),
            CenterY()
        )
        
        posterImageView.easy.layout(
            Leading(10),
            Height(200),
            Width(135),
            Top()
        )
        
        titlelabel.easy.layout(
            CenterY(-50).to(posterImageView),
            Leading(20).to(posterImageView, .trailing),
            Trailing(18)
        )
        
        genreLabel.easy.layout(
            CenterX().to(titlelabel),
            Top().to(titlelabel)
        )
        
        overvirewPlaceHolderLabel.easy.layout(
            Top(20).to(posterImageView),
            Leading().to(posterImageView, .leading)
        )
        
        summaryLabel.easy.layout(
            Top(10).to(overvirewPlaceHolderLabel),
            Leading().to(overvirewPlaceHolderLabel, .leading),
            Trailing(10)
        )
        
        youtubeView.easy.layout(
            Top(25).to(summaryLabel),
            Height(190),
            Leading(30),
            Trailing(30)
        )
        
        sepView.easy.layout(
            Top().to(youtubeView),
            Leading(40),
            Height(1),
            Trailing(40)
        )
        
        castPlaceHolderLabel.easy.layout(
            Top(20).to(sepView),
            Leading(15)
        )
        
        castView.easy.layout(
            Top(10).to(castPlaceHolderLabel),
            Leading(),
            Trailing(),
            Height(200)
        )
        
        secSepView.easy.layout(
            Top(20).to(castView),
            Leading(20),
            Height(1),
            Trailing(20)
        )
        
        suggestionPlaceHolderLabel.easy.layout(
            Top(20).to(secSepView),
            Leading(15)
        )
        
        suggestionView.easy.layout(
            Top(-5).to(suggestionPlaceHolderLabel),
            Leading(),
            Trailing(),
            Height(200),
            Bottom()
        )
    }
    
    // MARK: - BindUI
    
    func bindUI() {
        youtubeView.delegate = self
        backdropImageView.load(url: ImageBuildPath.getImageBackDrop(imagePath: dataResult?.backdropPath ?? ""))
        posterImageView.load(url: ImageBuildPath.getImages(imagePath: dataResult?.posterPath ?? ""))
        titlelabel.attributedText = NSAttributedString.textBold(dataResult?.title ?? "", color: .white, size: 22)
        
        summaryLabel.attributedText = NSAttributedString.regular(dataResult?.overview ?? "", color: .white, size: 12, alignment: .left)
        
        leftNavBarButton.rx.tap
            .subscribeOn(ConcurrentDispatchQueueScheduler(queue: .global()))
            .asDriverOnErrorJustComplete()
            .throttle(.microseconds(500), latest: false)
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                self.popViewController(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }
    
    func bindGenresName(_ genreID: Int) {
        GerneIDRequest.getGerneID(completion: { [weak self] data in
            guard let self = self else { return }
            for i in data.genres {
                if i.id == genreID {
                    self.genreName = i.name
                }
            }
        })
    }
    
    func getVideo(movieId: Int) {
        VideoRequest.getVideos(
            movieID: movieId,
            completion: { [weak self] data in
                guard let self = self else { return }
                print(data.results[0].key)
                let key = data.results.first!.key
                self.youtubeView.load(withVideoId: key,
                                      playerVars: ["playsinline": 1])
               
            }
        )
    }
    
    func bindViewModel() {
        RecommendationsRequest.getPopularMovies(movieID: dataResult?.id ?? 0, completion: { [weak self] data in
            guard let self = self else { return }
            self.suggestionView.setContent(source: data.results)
        })
        
        CastRequest.getPopularMovies(movieID: dataResult?.id ?? 0, completion: { [weak self] data in
            guard let self = self else { return }
            self.castView.setContent(source: data.cast)
        })
    }
}
