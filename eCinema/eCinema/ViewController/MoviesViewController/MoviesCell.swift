//
//  MoviesCell.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import UIKit

@available(iOS 11.0, *)
class MoviesCell: UITableViewCell {
    public static let identifier = "MoviesCell"
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .darkGreyNew
        view.layer.cornerRadius = 8
        view.applyShadow(withOpacity: 0.3, shadowSide: .all, shadowRadius: 7, color: UIColor.gold)
        return view
    }()
    
    private var posterImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 8
        image.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        return image
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private let voteAvarageimageView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.cornerRadius = 21
        view.clipsToBounds = false
        view.applyShadow(withOpacity: 0.6, shadowSide: .all, shadowRadius: 4, color: UIColor.gold)
        return view
    }()
    
    private let voteAvarageLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private let movieYearLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private let likeView: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "like").withRenderingMode(.alwaysTemplate))
        view.clipsToBounds = true
        view.tintColor = UIColor.gold
//        view.layer.cornerRadius = 9
        return view
    }()
    
    private let ratingLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        selectionStyle = .none
        
        addSubview(containerView)
        
        containerView.addSubview(titleLabel)
        containerView.addSubview(voteAvarageimageView)
        voteAvarageimageView.addSubview(voteAvarageLabel)
        containerView.addSubview(movieYearLabel)
        containerView.addSubview(likeView)
        containerView.addSubview(ratingLabel)
        containerView.addSubview(posterImageView)
    }
    
    private func setupLayout() {
        containerView.easy.layout(
            Top(10),
            Bottom(10),
            Leading(10),
            Trailing(10)
        )
        
        posterImageView.easy.layout(
            Leading(),
            Height(180),
            Width(115),
            Top(),
            Bottom()
        )
        
        titleLabel.easy.layout(
            Top(20),
            Leading(20).to(posterImageView, .trailing),
            Trailing(20)
        )
        
        voteAvarageimageView.easy.layout(
            Bottom(28).to(posterImageView, .bottom),
            Leading(-15).to(posterImageView),
            Size(42)
        )
        
        voteAvarageLabel.easy.layout(
            Leading(13),
            Trailing(),
            Center()
        )
        
        movieYearLabel.easy.layout(
            Top().to(titleLabel),
            Leading().to(titleLabel, .leading),
            Trailing()
        )
        
        likeView.easy.layout(
            Size(15),
            CenterY().to(ratingLabel),
            Trailing(6).to(ratingLabel)
        )
        
        ratingLabel.easy.layout(
            Bottom(10),
            Trailing(10)
        )
    }
    
    func setContent(imageURL: URL, title: String, vote: Double, date: String, rate: Int) {
        posterImageView.load(url: imageURL)
        titleLabel.attributedText = NSAttributedString.textBold(title, color: .white, size: 17, alignment: .left)
        voteAvarageLabel.attributedText = NSAttributedString.textBold("\(vote)", color: UIColor.gold, size: 13, alignment: .center)
        movieYearLabel.attributedText = NSAttributedString.regular(date, color: UIColor.gold, size: 10, alignment: .left)
        ratingLabel.attributedText = NSAttributedString.regular("\(rate)", color: .white, size: 14)
    }
}
