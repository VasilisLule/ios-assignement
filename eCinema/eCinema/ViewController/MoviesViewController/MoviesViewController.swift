//
//  MoviesViewController.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import EasyPeasy
import RxSwift
import UIKit

@available(iOS 11.0, *)
class MoviesViewController: BaseViewController {
    // MARK: - Properties
    
    private var popularMoviesModel: PopularMoviesModel?
    private var listOfMovies: [Result] = []
    private var fetchImageSource: [URL] = []
    
    var movieOverviewViewController: MovieOverviewViewController?
    
    // MARK: - UI Properties
    
    private let containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.isScrollEnabled = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        tableView.register(MoviesCell.self, forCellReuseIdentifier: MoviesCell.identifier)
        
        return tableView
    }()
    
    private lazy var searchView: SearchTextFieldView = {
        var view = SearchTextFieldView()
        view.layer.cornerRadius = 8
        view.applyShadow(withOpacity: 0.2, shadowSide: .bottom, shadowRadius: 7, color: UIColor.gold)
        return view
    }()
    
    // MARK: - ViewController LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    // MARK: - Setup
    
    override func setupView() {
        super.setupView()
        view.backgroundColor = UIColor.darkGreyNew
        view.addSubview(containerView)
        containerView.addSubview(tableView)
        containerView.addSubview(searchView)
        setupNavBar(withLeftButtonType: nil, withRightButtonType: nil)
        
        bindUI()
        bindViewModel()
    }
    
    override func setupLayout() {
        super.setupLayout()
        
        containerView.easy.layout(
            Edges()
        )
        
        searchView.easy.layout(
            Top(20).to(navBarWrapperView),
            Leading(20),
            Trailing(20),
            Height(36)
        )
        
        tableView.easy.layout(
            Top().to(searchView),
            Leading(20),
            Trailing(20),
            Bottom()
        )
    }
    
    func bindUI() {
        searchView.searchTextField.delegate = self
    }
    
    var isSearchPriority = false
    
    func searchMovies() {
        searchView.searchTextField.resignFirstResponder()
        
        guard let text = searchView.searchTextField.text, !text.isEmpty else { return }
        
        SearchRequest.getSearch(searchText: text, completion: { data in
            
            self.listOfMovies.removeAll()
            self.listOfMovies = data.results
            
            self.fetchImageSource.removeAll()
            data.results.forEach { value in
                let imageURL = ImageBuildPath.getImages(imagePath: value.posterPath ?? "")
                self.fetchImageSource.append(imageURL)
                print(imageURL)
            }
            
            self.tableView.reloadData()
            self.isSearchPriority = true
        })
    }
    
    // MARK: - BindViewModel
    
    func bindViewModel() {
        PopularMoviesRequest.getPopularMovies(completion: { [weak self] data in
            guard let self = self else { return }
            
            self.popularMoviesModel = data
            self.listOfMovies = data.results
            
            self.fetchImageSource.removeAll()
            
            data.results.forEach { value in
                let imageURL = ImageBuildPath.getImages(imagePath: value.posterPath ?? "")
                self.fetchImageSource.append(imageURL)
                print(imageURL)
            }
            self.tableView.reloadData()
            self.isSearchPriority = false
        })
    }
    
    func refreshData() {
        if isSearchPriority {
            return
        }
        
        let index = listOfMovies.count
        let batchSize = index + 20
        let currentPage = batchSize / 20
        if index < batchSize {
            PopularMoviesRequest.getPopularMovies(pages: currentPage, completion: { [weak self] data in
                guard let self = self else { return }
                
                self.popularMoviesModel = data
                self.listOfMovies.append(contentsOf: data.results)
                data.results.forEach { value in
                    let imageURL = ImageBuildPath.getImages(imagePath: value.posterPath ?? "")
                    self.fetchImageSource.append(imageURL)
                }
                self.tableView.reloadData()
            })
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchMovies()
        self.tableView.reloadData()
        return true
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.tableView.reloadData()
        if textField.text!.isEmpty {
            bindViewModel()
        }
    }
}

@available(iOS 11.0, *)
extension MoviesViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if searchView.searchTextField.isEditing {
            view.endEditing(true)
        }
    }
}

@available(iOS 11.0, *)
extension MoviesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MoviesCell.identifier, for: indexPath) as! MoviesCell
        cell.setContent(imageURL: fetchImageSource[indexPath.row], title: listOfMovies[indexPath.row].title, vote: listOfMovies[indexPath.row].voteAverage ?? 0.0, date: listOfMovies[indexPath.row].releaseDate ?? "", rate: listOfMovies[indexPath.row].voteCount ?? Int(0))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchView.searchTextField.isEditing {
            view.endEditing(true)
            return
        }
        
        movieOverviewViewController = MovieOverviewViewController(dataResult: listOfMovies[indexPath.row])
        guard let movieOverViewVC = movieOverviewViewController else { return }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(movieOverViewVC, animated: true)
        }
        
        movieOverviewViewController = nil
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == listOfMovies.count - 1 {
            if listOfMovies.count < popularMoviesModel?.totalPages ?? 1 {
                refreshData()
            }
        }
    }
}
