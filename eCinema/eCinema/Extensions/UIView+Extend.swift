//
//  UIView+Extend.swift
//  eCinema
//
//  Created by Vasilis Lule on 26/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit

public extension UIView {
    
    enum ShadowSideViewType {
        case all
        case top
        case right
        case bottom
        case left
    }
    
    func applyShadow(withOpacity opacity: Float = 0.05, shadowSide side: ShadowSideViewType = .all, shadowRadius radius: CGFloat = 5, color: UIColor = .black ) {
        layer.shadowColor = color.cgColor//UIColor.color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        var width = 0
        var height = 0
        switch side {
        case .all:
            width = 0
            height = 0
        case .top:
            width = 0
            height = -7
        case .right:
            width = 7
            height = 0
        case .bottom:
            width = 0
            height = 7
        case .left:
            width = -7
            height = 0
        }
        layer.shadowOffset = CGSize(width: width, height: height)
    }
    
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        if let image = UIGraphicsGetImageFromCurrentImageContext(), let pngImageData =  image.pngData(), let pngSmallImage = UIImage(data: pngImageData) {
            UIGraphicsEndImageContext()
            return pngSmallImage
        }
        print("image could'nt be created")
        return UIImage()
    }
    
}
