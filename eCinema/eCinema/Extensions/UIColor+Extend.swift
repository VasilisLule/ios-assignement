//
//  UIColor+Extend.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Foundation
import SwifterSwift

extension UIColor {
    func toImage() -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0)
        self.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    static let orange:UIColor! = UIColor(hexString: "FF9801", transparency: 1)
    static let darkGreyNew:UIColor! = UIColor(hexString: "1D1E1E", transparency: 1.0)
    static let lightGrey:UIColor! = UIColor(hexString: "292A2A", transparency: 1.0)
    static let premiumShadow:UIColor! = UIColor(hexString: "2A2A2A", transparency: 0.8)
    static let gold:UIColor! = UIColor(hexString: "8F7A49", transparency: 1)
}
