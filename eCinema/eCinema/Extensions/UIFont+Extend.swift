//
//  UIFont+Extend.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit

public struct FontTypes {
    static let ArialRoundedMTBold = "ArialRoundedMTBold"
    static let Regular = "OpenSans-Regular"
}

public extension UIFont {
    
    //eMobility
    class func fontSizeWithScreenSizeRelation(fontSize size: CGFloat) -> CGFloat {
        let designedScreenValue: CGFloat = 812
        let screenValue: CGFloat = UIScreen.main.bounds.height >= 812 ? 812 : UIScreen.main.bounds.height
        let percentForDesignedValue: CGFloat = size/designedScreenValue
        
        let newValue = screenValue*percentForDesignedValue
        return newValue
    }
    
    //MARK: - Fonts
    //Shouthen
//    class func arialRoundedMTBold(withSize size : NSNumber) -> UIFont {
//        return UIFont(name:FontTypes.ArialRoundedMTBold, size: CGFloat(truncating: size))!
//    }
    
    class func arialRoundedMTBold(withSize size: CGFloat) -> UIFont {
        return UIFont(name: FontTypes.ArialRoundedMTBold, size: UIFont.fontSizeWithScreenSizeRelation(fontSize: size))!
    }
    
    //eMobility
    class func regular(withSize size: CGFloat) -> UIFont {
        return UIFont(name: FontTypes.Regular, size: UIFont.fontSizeWithScreenSizeRelation(fontSize: size))!
    }
}
