//
//  ExtendedTouchAreaButton.swift
//  eCinema
//
//  Created by Vasilis Lule on 26/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit

public class ExtendedTouchAreaButton: UIButton {
    
    var addedTouchArea = CGFloat(22)
    
    override public func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        
        let newBound = CGRect(
            x: self.bounds.origin.x - addedTouchArea,
            y: self.bounds.origin.y - addedTouchArea,
            width: self.bounds.width + 2 * addedTouchArea,
            height: self.bounds.height + 2 * addedTouchArea
        )
        return newBound.contains(point)
    }
}
