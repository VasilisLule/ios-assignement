//
//  NSAttributedString+Extend.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit

struct Variables {
    static let LETTERSPACING_NO:Float = 0
    static let LETTERSPACING_SMALL:Float = 0.167
    static let LETTERSPACING_BIG:Float = 0.333
}

public extension NSAttributedString {
    
    class func textBold(_ string : String, color : UIColor = .white, size : NSNumber? = 21.0, lineSpacing: CGFloat = 0,
    alignment: NSTextAlignment = .center) -> NSAttributedString {
        var color = color, size = size
        
        if size == nil {
            size = NSNumber(value: 21.0 as Float)
        }
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing/8
        style.alignment = alignment
        style.lineBreakMode = .byWordWrapping
        
        return NSAttributedString(string: string, attributes:
            [NSAttributedString.Key.foregroundColor:color,
             NSAttributedString.Key.font:UIFont.arialRoundedMTBold(withSize: size as! CGFloat),
             NSAttributedString.Key.kern: Variables.LETTERSPACING_BIG,
             NSAttributedString.Key.paragraphStyle: style])
    }
    
    class func regular(_ string : String,
                       color: UIColor,
                    size : CGFloat,
                    kern: CGFloat = 0,
                    lineSpacing: CGFloat = 0,
                    alignment: NSTextAlignment = .center) -> NSAttributedString {
        let font = UIFont.regular(withSize: size)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing/8
        style.alignment = alignment
        style.lineBreakMode = .byTruncatingTail
        
        return NSAttributedString(
            string: string,
            attributes:
            [NSAttributedString.Key.foregroundColor: color,
             NSAttributedString.Key.font: font,
             NSAttributedString.Key.kern: kern,
             NSAttributedString.Key.paragraphStyle: style]
        )
    }
    
    class func regularTextAttributes(color: UIColor,
                                     size : CGFloat,
                                     kern: CGFloat = 0,
                                     lineSpacing: CGFloat = 0,
                                     alignment: NSTextAlignment = .center) -> [NSAttributedString.Key : Any] {
        let font = UIFont.regular(withSize: size)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing/8
        style.alignment = alignment
        style.lineBreakMode = .byTruncatingTail
        
        return [NSAttributedString.Key.foregroundColor: color,
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.kern: kern,
                NSAttributedString.Key.paragraphStyle: style]
    }
}
