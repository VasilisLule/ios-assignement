//
//  SearchTextFieldView.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit
import EasyPeasy

class SearchTextFieldView: UIView {
    
    //MARK: - Properties
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.cornerRadius = 8
        return view
    }()
    
    public lazy var searchTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        
        textField.backgroundColor = UIColor.lightGray
        textField.attributedPlaceholder = NSAttributedString.regular("Search", color: UIColor.gray, size: 16, kern: 0, lineSpacing: 0, alignment: .left)
        textField.defaultTextAttributes = NSAttributedString.regularTextAttributes(color: UIColor.black, size: 16, kern: 0, lineSpacing: 0, alignment: .left)
        
        let imageView = UIImageView(frame: CGRect(x: -12, y: 3, width: 10, height: 15))
        imageView.image = #imageLiteral(resourceName: "search")
        imageView.sizeToFit()
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        view.addSubview(imageView)
        
        textField.leftView = view
        textField.leftViewMode = .always
        return textField
    }()
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        
        addSubview(containerView)
        containerView.addSubview(searchTextField)
    }
    
    private func setupLayout() {
        
        containerView.easy.layout(
            Edges()
        )
        
        searchTextField.easy.layout(
            Top(5),
            Leading(20),
            Bottom(5),
            Trailing(20)
        )
    }
}
