//
//  SeperatorView.swift
//  eCinema
//
//  Created by Vasilis Lule on 26/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit


class SeperatorView: UIView {
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = UIColor.lightGrey
    }
}

