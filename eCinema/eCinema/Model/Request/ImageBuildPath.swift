//
//  ImageBuildPath.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Foundation

class ImageBuildPath {
    static func getImages(imagePath: String) -> URL {
        let imageURL = "https://image.tmdb.org/t/p/w200/\(imagePath)"
        
        var imageUrl: URL?
        if let url = URL(string: imageURL) {
            imageUrl = url
        }
        
        return imageUrl ?? URL(string: "")!
    }
    
    static func getImageBackDrop(imagePath: String) -> URL {
        let imageURL = "https://image.tmdb.org/t/p/w300/\(imagePath)"
        
        var imageUrl: URL?
        if let url = URL(string: imageURL) {
            imageUrl = url
        }
        
        return imageUrl ?? URL(string: "")!
    }
}
