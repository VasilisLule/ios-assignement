//
//  VideoRequest.swift
//  eCinema
//
//  Created by Vasilis Lule on 30/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Foundation
import Alamofire


class VideoRequest {
    
    //https://api.themoviedb.org/3/movie/419704/videos?api_key=6b2e856adafcc7be98bdf0d8b076851c&language=en-US&=&=
    static let API = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseUrl = "https://api.themoviedb.org/3/movie/"
    
    static let secondPart = "/videos?api_key=\(API)&language=en-US&=&="
    
    static func getVideos(movieID: Int, completion: @escaping (_ result: VideoModel) -> Void) {
        
        let request = "\(baseUrl)\(movieID)\(secondPart)"
        
        print(request)
        
        AF.request(request, method: .get,
                   parameters: nil,
                   encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response { response in
                    guard let data = response.data else { return }
                    
                    DispatchQueue.main.async {
                        do {
                            let video = try JSONDecoder().decode(VideoModel.self, from: data)
                            completion(video)
                        } catch {
                            print("Error decoding", error)
                        }
                    }
        }
    }
}
