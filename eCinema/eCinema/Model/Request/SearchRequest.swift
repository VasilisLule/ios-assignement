//
//  SearchRequest.swift
//  eCinema
//
//  Created by Vasilis Lule on 29/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Alamofire
import Foundation
import RxSwift

class SearchRequest {
    static let API = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseUrl = "https://api.themoviedb.org/3/search/movie?api_key=\(API)&language=en-US&query="
    static let pageUrl = "&page="

    static func getSearch(searchText: String, pages: Int = 1, completion: @escaping (_ result: PopularMoviesModel) -> Void) {
        let handleStringSpace = searchText.replacingOccurrences(of: " ", with: "%20")

        let request = "\(SearchRequest.baseUrl)\(handleStringSpace)\(pageUrl)\(String(pages))"

        print(request)

        AF.request(request, method: .get,
                   parameters: nil,
                   encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response { response in
            guard let data = response.data else { return }

            DispatchQueue.main.async {
                do {
                    let movies = try JSONDecoder().decode(PopularMoviesModel.self, from: data)
                    completion(movies)
                } catch {
                    print("Error decoding", error)
                }
            }
        }
    }
}
