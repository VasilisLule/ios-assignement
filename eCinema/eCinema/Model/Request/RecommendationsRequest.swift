//
//  RecommendationsRequest.swift
//  eCinema
//
//  Created by Vasilis Lule on 28/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Alamofire
import Foundation

class RecommendationsRequest {
    static let API = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseUrl = "https://api.themoviedb.org/3/movie/"
    static let secondPartUrl = "/recommendations?api_key=\(API)&language=en-US&page="
    
    static func getPopularMovies(movieID: Int, pages: Int = 1, completion: @escaping (_ result: RecommendationsModel) -> Void) {
        let request = "\(baseUrl)\(movieID)\(secondPartUrl)\(String(pages))"
        
        print(request)
        
        AF.request(request, method: .get,
                   parameters: nil,
                   encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response { response in
            guard let data = response.data else { return }
            
            DispatchQueue.main.async {
                do {
                    let recommendations = try JSONDecoder().decode(RecommendationsModel.self, from: data)
                    completion(recommendations)
                } catch {
                    print("Error decoding", error)
                }
            }
        }
    }
}
