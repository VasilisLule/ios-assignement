//
//  CastRequest.swift
//  eCinema
//
//  Created by Vasilis Lule on 28/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Alamofire
import Foundation

class CastRequest {
    static let API = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseUrl = "https://api.themoviedb.org/3/movie/"
    static let secondPartUrl = "/credits?api_key=\(API)"
    
    static func getPopularMovies(movieID: Int, completion: @escaping (_ result: CastModel) -> Void) {
        let request = "\(baseUrl)\(movieID)\(secondPartUrl)"
        
        print(request)
        
        AF.request(request, method: .get,
                   parameters: nil,
                   encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response { response in
            guard let data = response.data else { return }
            
            DispatchQueue.main.async {
                do {
                    let cast = try JSONDecoder().decode(CastModel.self, from: data)
                    completion(cast)
                } catch {
                    print("Error decoding", error)
                }
            }
        }
    }
}
