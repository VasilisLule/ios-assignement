//
//  PopularMoviesRequest.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit
import Alamofire

class PopularMoviesRequest {
    
    static let API = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseUrl = "https://api.themoviedb.org/3/movie/popular?api_key=\(API)&language=en-US&page="
    
    static func getPopularMovies(pages: Int = 1, completion: @escaping (_ result: PopularMoviesModel) -> Void) {
        
        let request = "\(PopularMoviesRequest.baseUrl)\(String(pages))"
        
        print(request)
        
        AF.request(request, method: .get,
                   parameters: nil,
                   encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response { response in
                    guard let data = response.data else { return }
                    
                    DispatchQueue.main.async {
                        do {
                            let movies = try JSONDecoder().decode(PopularMoviesModel.self, from: data)
                            completion(movies)
                        } catch {
                            print("Error decoding", error)
                        }
                    }
        }
    }
}
