//
//  GerneIDRequest.swift
//  eCinema
//
//  Created by Vasilis Lule on 28/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import Alamofire
import Foundation

class GerneIDRequest {
    static let API = "6b2e856adafcc7be98bdf0d8b076851c"
    static let baseUrl = "https://api.themoviedb.org/3/genre/movie/list?api_key=\(API)&language=en-US"
    
    static func getGerneID(completion: @escaping (_ result: GerneIDModel) -> Void) {
        AF.request(baseUrl, method: .get,
                   parameters: nil,
                   encoding: URLEncoding.default,
                   headers: nil, interceptor: nil).response { response in
            guard let data = response.data else { return }
            
            DispatchQueue.main.async {
                do {
                    let gerne = try JSONDecoder().decode(GerneIDModel.self, from: data)
                    completion(gerne)
                } catch {
                    print("Error decoding", error)
                }
            }
        }
    }
}
