//
//  CastModel.swift
//  eCinema
//
//  Created by Vasilis Lule on 28/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let castModel = try? newJSONDecoder().decode(CastModel.self, from: jsonData)

import Foundation

// MARK: - CastModel

class CastModel: Codable {
    let id: Int
    let cast: [Cast]
}

// MARK: - Cast

class Cast: Codable {
    let castID: Int
    let character, creditID: String
    let gender, id: Int
    let name: String
    let order: Int
    let profilePath: String?

    enum CodingKeys: String, CodingKey {
        case castID = "cast_id"
        case character
        case creditID = "credit_id"
        case gender, id, name, order
        case profilePath = "profile_path"
    }
}
