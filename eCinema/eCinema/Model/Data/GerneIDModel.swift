//
//  GerneIDModel.swift
//  eCinema
//
//  Created by Vasilis Lule on 28/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let gerneIDModel = try? newJSONDecoder().decode(GerneIDModel.self, from: jsonData)

import Foundation

// MARK: - GerneIDModel

class GerneIDModel: Codable {
    let genres: [Genre]
}

// MARK: - Genre

class Genre: Codable {
    let id: Int
    let name: String
}
