//
//  RecommendationsModel.swift
//  eCinema
//
//  Created by Vasilis Lule on 28/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let recommendationsModel = try? newJSONDecoder().decode(RecommendationsModel.self, from: jsonData)

import Foundation

// MARK: - RecommendationsModel

class RecommendationsModel: Codable {
    let page: Int
    let results: [ResultRecomend]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

// MARK: - Result

class ResultRecomend: Codable {
    let id: Int
    let video: Bool?
    let voteCount: Int
    let voteAverage: Double
    let title, releaseDate: String
    let originalTitle: String
    let genreIDS: [Int]
    let backdropPath: String?
    let adult: Bool
    let overview, posterPath: String
    let popularity: Double

    enum CodingKeys: String, CodingKey {
        case id, video
        case voteCount = "vote_count"
        case voteAverage = "vote_average"
        case title
        case releaseDate = "release_date"
        case originalTitle = "original_title"
        case genreIDS = "genre_ids"
        case backdropPath = "backdrop_path"
        case adult, overview
        case posterPath = "poster_path"
        case popularity
    }
}
