//
//  AppDelegate.swift
//  eCinema
//
//  Created by Vasilis Lule on 25/6/20.
//  Copyright © 2020 Vasilis Lule. All rights reserved.
//

import UIKit
import SwiftUI

@available(iOS 11.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        sleep(1)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let rootVC = MoviesViewController() //MovieOverviewViewController()//MoviesViewController()
        let navigationController = UINavigationController(rootViewController: rootVC)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
    
    
}

